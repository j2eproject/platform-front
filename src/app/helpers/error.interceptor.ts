import { Injectable, ErrorHandler } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private toastr: ToastrService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401 && !err.url.includes('login')) {
                // auto logout if 401 response returned from api
                this.authenticationService.logout();
                location.reload();
            }

            // Body mal formé (Bad Request)
            if (err.status === 400) {
                this.errorHandler(err.error);
            }

            // Interdit (Forbidden)
            if (err.status === 403) {
                this.toastr.error('Accès interdit');
            }

            // Non trouvé (Not Found)
            if (err.status === 404) {
                this.errorHandler(err.error);
            }

            // Conflit (Conflict)
            if (err.status === 409) {
                this.errorHandler(err.error);
            }

            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }

    errorHandler(err) {
        if (typeof(err) === 'string') {
            this.toastr.error(err);
            return;
        } else {
            if (Array.isArray(err.errors)) {
                err.errors.map(o => {
                    this.toastr.error(o.defaultMessage);
                });
            } else {
                this.toastr.error(err.message);
            }
        }
    }
}
