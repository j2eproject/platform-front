import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, private configService: ConfigService) { }

  getUsers(): Promise<any> {
    return this.http.get<any>(`${this.configService.platformApi()}/users`).toPromise();
  }

  register(mail: string, firstname: string, lastname: string, password: string, confirmPassword: string): Promise<any> {
    return this.http.post<any>(`${this.configService.platformApi()}/users`,
      {
        mail, firstname, lastname, password, confirmPassword
      }).toPromise();
  }

  getUserById(id: number): Promise<any> {
    return this.http.get<any>(`${this.configService.platformApi()}/users/${id}`).toPromise();
  }

  editUser(id: number, firstname: string, lastname: string, role: string): Promise<any> {
    return this.http.put<any>(`${this.configService.platformApi()}/users/${id}`,
      {
        firstname, lastname, role
      }).toPromise();
  }

  deleteUser(id: number): Promise<any> {
    return this.http.delete<any>(`${this.configService.platformApi()}/users/${id}`).toPromise();
  }

  editPasswordUser(id: number, password: string, confirmPassword: string): Promise<any> {
    return this.http.patch<any>(`${this.configService.platformApi()}/users/${id}`,
      {
        password, confirmPassword
      }).toPromise();
  }

  exportPdf() {
    const headers = new HttpHeaders();
    headers.set('Accept', 'application/pdf');
    return this.http.get<any>(`${this.configService.platformApi()}/users/pdfreport`,
      {
        headers,
        responseType: 'blob' as 'json'
      }).subscribe(
        (response) => {
          saveAs(response, 'users_report.pdf');
        }
      );
  }

  exportCsv() {
    const headers = new HttpHeaders();
    headers.set('Accept', 'text/csv');
    return this.http.get<any>(`${this.configService.platformApi()}/users/csvreport`,
      {
        headers,
        responseType: 'blob' as 'json'
      }).subscribe(
        (response) => {
          saveAs(response, 'users_report.csv');
        }
      );
  }

  exportJson() {
    const headers = new HttpHeaders();
    headers.set('Accept', 'text/json');
    return this.http.get<any>(`${this.configService.platformApi()}/users/jsonreport`,
      {
        headers,
        responseType: 'blob' as 'json'
      }).subscribe(
        (response) => {
          saveAs(response, 'users_report.json');
        }
      );
  }
}
