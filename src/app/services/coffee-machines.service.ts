import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class CoffeeMachinesService {

  constructor(private http: HttpClient, private configService: ConfigService) { }

  getCoffeeMachines(): Promise<any> {
    return this.http.get<any>(`${this.configService.platformApi()}/coffeemachines`).toPromise();
  }

  getCoffeeMachineById(id: number): Promise<any> {
    return this.http.get<any>(`${this.configService.platformApi()}/coffeemachines/${id}`).toPromise();
  }

  createCoffeeMachine(port: number, url: string): Promise<any> {
    return this.http.post<any>(`${this.configService.platformApi()}/coffeemachines`,
      {
        port, url
      }).toPromise();
  }

  editCoffeeMachine(id: number, port: number, url: string): Promise<any> {
    return this.http.put<any>(`${this.configService.platformApi()}/coffeemachines/${id}`,
      {
        port, url
      }).toPromise();
  }

  deleteCoffeeMachine(id: number): Promise<any> {
    return this.http.delete<any>(`${this.configService.platformApi()}/coffeemachines/${id}`).toPromise();
  }
}
