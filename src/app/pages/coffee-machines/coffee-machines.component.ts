import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoffeeMachinesService } from 'src/app/services/coffee-machines.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-coffee-machines',
  templateUrl: './coffee-machines.component.html',
  styleUrls: ['./coffee-machines.component.scss']
})
export class CoffeeMachinesComponent implements OnInit {
  dataSource: any;
  displayedColumns =  ['id', 'port', 'url', 'status', 'version', 'stock', 'actions'];
  coffeeMachineSelected: any;
  createSubmitted = false;
  editSubmitted = false;
  createForm: FormGroup;
  editForm: FormGroup;
  config = {};
  loading = false;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private coffeeMachineService: CoffeeMachinesService,
              private authService: AuthenticationService,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.config = {
      animationType: 'circleSwish',
      backdropBorderRadius: '3px'
    };
    this.fillTable();
  }

  fillTable() {
    this.coffeeMachineService.getCoffeeMachines()
    .then((coffeeMachines) => {
      this.dataSource = new MatTableDataSource(coffeeMachines);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
    .catch((err) => {
    });
  }

  // convenience getter for easy access to form fields
  get cf() { return this.createForm.controls; }
  get ef() { return this.editForm.controls; }

  openCreateModal(createModal) {
    this.modalService.open(createModal);
    this.createForm = this.formBuilder.group({
      port: ['', Validators.required],
      url: ['', Validators.required]
    });
  }

  openEditModal(coffeeMachine, editModal) {
    this.coffeeMachineSelected = coffeeMachine;
    this.editForm = this.formBuilder.group({
      port: [this.coffeeMachineSelected.port, Validators.required],
      url: [this.coffeeMachineSelected.url, Validators.required]
    });
    this.modalService.open(editModal);
  }

  openDeleteModal(coffeeMachine, deleteModal) {
    this.coffeeMachineSelected = coffeeMachine;
    this.modalService.open(deleteModal);
  }

  isAdmin() {
    return this.authService.hasRole('ADMIN');
  }

  onCreateSubmit() {
    this.createSubmitted = true;

    // stop here if form is invalid
    if (this.createForm.invalid) {
      return;
    }

    this.loading = true;
    this.coffeeMachineService.createCoffeeMachine(this.cf.port.value, this.cf.url.value)
      .then(() => {
        this.fillTable();
        this.loading = false;
        this.createSubmitted = false;
        this.modalService.dismissAll();
        this.toastr.success('La machine à café a été correctement ajoutée');
      })
      .catch((error) => {
        this.loading = false;
      });
  }

  onEditSubmit() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    this.loading = true;
    this.coffeeMachineService.editCoffeeMachine(this.coffeeMachineSelected.id, this.ef.port.value, this.ef.url.value)
    .then((coffeeMachine) => {
      this.loading = false;
      this.editSubmitted = false;
      this.fillTable();
      this.modalService.dismissAll();
      this.toastr.success('La machine à café a été correctement modifiée');
    }).catch((err) => {
      this.loading = false;
    });
  }

  onDeleteSubmit() {
    this.loading = true;
    this.coffeeMachineService.deleteCoffeeMachine(this.coffeeMachineSelected.id).then((coffeeMachine) => {
      this.loading = false;
      this.fillTable();
      this.modalService.dismissAll();
      this.toastr.success('La machine à café a été correctement supprimée');
    }).catch((err) => {
      this.loading = false;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
