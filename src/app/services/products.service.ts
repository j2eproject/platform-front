import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient, private configService: ConfigService) { }

  getProducts(coffeeMachineId: number): Promise<any> {
    return this.http.get<any>(`${this.configService.platformApi()}/coffeemachines/${coffeeMachineId}/products`).toPromise();
  }

  createProduct(coffeeMachineId: number, libelle: string, quantity): Promise<any> {
    console.log(quantity);
    return this.http.post<any>(`${this.configService.platformApi()}/coffeemachines/${coffeeMachineId}/products`,
      {
        libelle,
        quantity
      }).toPromise();
  }

  deleteProduct(coffeeMachineId: number, libelle: string, quantity: number): Promise<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: { quantity, libelle }
    };
    return this.http.delete<any>(`${this.configService.platformApi()}/coffeemachines/${coffeeMachineId}/products/${libelle}`,
      httpOptions).toPromise();
  }

  exportPdf(coffeeMachineId: number) {
    const headers = new HttpHeaders();
    headers.set('Accept', 'application/pdf');
    return this.http.get<any>(`${this.configService.platformApi()}/coffeemachines/${coffeeMachineId}/products/pdfreport`,
      {
        headers,
        responseType: 'blob' as 'json'
      }).subscribe(
        (response) => {
          saveAs(response, `coffee_machine_${coffeeMachineId}_products_report.pdf`);
        }
      );
  }

  exportCsv(coffeeMachineId: number) {
    const headers = new HttpHeaders();
    headers.set('Accept', 'text/csv');
    return this.http.get<any>(`${this.configService.platformApi()}/coffeemachines/${coffeeMachineId}/products/csvreport`,
      {
        headers,
        responseType: 'blob' as 'json'
      }).subscribe(
        (response) => {
          saveAs(response, `coffee_machine_${coffeeMachineId}_products_report.csv`);
        }
      );
  }

  exportJson(coffeeMachineId: number) {
    const headers = new HttpHeaders();
    headers.set('Accept', 'text/json');
    return this.http.get<any>(`${this.configService.platformApi()}/coffeemachines/${coffeeMachineId}/products/jsonreport`,
      {
        headers,
        responseType: 'blob' as 'json'
      }).subscribe(
        (response) => {
          saveAs(response, `coffee_machine_${coffeeMachineId}_products_report.json`);
        }
      );
  }
}
