import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  config = {};

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
    private router: Router,
    private usersService: UsersService) { }

  ngOnInit() {
    this.config = {
      animationType: 'circleSwish',
      backdropBorderRadius: '3px'
    };

    this.registerForm = this.formBuilder.group({
      mail: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
    // reset login status
    this.authenticationService.logout();
    this.router.navigate(['/register']);
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.usersService.register(this.f.mail.value, this.f.firstname.value,
      this.f.lastname.value, this.f.password.value, this.f.confirmPassword.value)
      .then(() => {
        this.router.navigate(['/login']);
      })
      .catch((error) => {
        this.loading = false;
      });
  }

}
