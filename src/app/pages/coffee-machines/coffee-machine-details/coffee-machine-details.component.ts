import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CoffeeMachinesService } from 'src/app/services/coffee-machines.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ProductsService } from 'src/app/services/products.service';


@Component({
  selector: 'app-coffee-machine-details',
  templateUrl: './coffee-machine-details.component.html',
  styleUrls: ['./coffee-machine-details.component.scss']
})
export class CoffeeMachineDetailsComponent implements OnInit {
  dataSource: any;
  displayedColumns = ['id', 'libelle', 'quantity', 'actions'];
  coffeeMachine: any;
  editSubmitted = false;
  createProductSubmitted = false;
  deleteProductSubmitted = false;
  editForm: FormGroup;
  createProductForm: FormGroup;
  deleteProductForm: FormGroup;
  productSelected: any;
  config = {};
  loading = false;
  exports = [
    {value: 'file-pdf', viewValue: 'PDF'},
    {value: 'file-csv', viewValue: 'CSV'},
    {value: 'code', viewValue: 'JSON'}
  ];
  currentExport = this.exports[0].value;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private route: ActivatedRoute,
              private coffeeMachineService: CoffeeMachinesService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private router: Router,
              private toastr: ToastrService,
              private productsService: ProductsService,
              private authService: AuthenticationService) { }

  ngOnInit() {
    this.config = {
      animationType: 'circleSwish',
      backdropBorderRadius: '3px'
    };
    const id = this.route.snapshot.params.id;
    this.refreshCoffeeMachine(id);
  }

  refreshCoffeeMachine(id: number) {
    this.coffeeMachine = this.coffeeMachineService.getCoffeeMachineById(id)
      .then((coffeeMachine) => {
        this.coffeeMachine = coffeeMachine;
        this.fillProductTable();
      }).catch((err) => {
        this.router.navigate(['/404']);
      });
  }

  fillProductTable() {
    this.productsService.getProducts(this.coffeeMachine.id)
    .then((products) => {
      this.dataSource = new MatTableDataSource(products);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
    .catch((err) => {
    });
  }

  get ef() { return this.editForm.controls; }
  get cpf() { return this.createProductForm.controls; }
  get dpf() { return this.deleteProductForm.controls; }

  openEditModal(editModal) {
    this.modalService.open(editModal);
    this.editForm = this.formBuilder.group({
      port: [this.coffeeMachine.port, Validators.required],
      url: [this.coffeeMachine.url, Validators.required]
    });
  }

  openDeleteModal(deleteModal) {
    this.modalService.open(deleteModal);
  }

  openCreateProductModal(createProductModal) {
    this.modalService.open(createProductModal);
    if (this.coffeeMachine.version === '1.1') {
      this.createProductForm = this.formBuilder.group({
        libelle: ['', Validators.required],
        quantity: ['', Validators.required]
      });
    } else {
      this.createProductForm = this.formBuilder.group({
        libelle: ['', Validators.required]
      });
    }
  }

  openDeleteProductModal(product, deleteProductModal) {
    this.productSelected = product;
    this.modalService.open(deleteProductModal);
    if (this.coffeeMachine.version === '1.1') {
      this.deleteProductForm = this.formBuilder.group({
        quantity: ['', Validators.required]
      });
    }
  }

  onEditSubmit() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    this.loading = true;
    this.coffeeMachineService.editCoffeeMachine(this.coffeeMachine.id, this.ef.port.value, this.ef.url.value)
      .then((coffeeMachine) => {
        this.loading = false;
        this.editSubmitted = false;
        this.coffeeMachine = coffeeMachine;
        this.modalService.dismissAll();
        this.toastr.success('La machine à café a été correctement modifiée');
      }).catch((err) => {
        this.loading = false;
      });
  }

  onDeleteSubmit() {
    this.loading = true;
    this.coffeeMachineService.deleteCoffeeMachine(this.coffeeMachine.id).then((coffeeMachine) => {
      this.loading = false;
      this.modalService.dismissAll();
      this.toastr.success('La machine à café a été correctement supprimée');
      this.router.navigate(['/coffeemachines']);
    }).catch((err) => {
      this.loading = false;
    });
  }

  onCreateProductSubmit() {
    this.createProductSubmitted = true;

    // stop here if form is invalid
    if (this.createProductForm.invalid) {
      return;
    }

    this.loading = true;
    const quantity = this.coffeeMachine.version === '1.1' ? this.cpf.quantity.value : 0;
    this.productsService.createProduct(this.coffeeMachine.id, this.cpf.libelle.value, quantity)
      .then(() => {
        this.fillProductTable();
        this.loading = false;
        this.createProductSubmitted = false;
        this.modalService.dismissAll();
        this.toastr.success('Le produit a été correctement ajouté');
        this.refreshCoffeeMachine(this.coffeeMachine.id);
      })
      .catch((error) => {
        this.loading = false;
        this.refreshCoffeeMachine(this.coffeeMachine.id);
        this.modalService.dismissAll();
      });
  }

  onDeleteProductSubmit() {
    this.deleteProductSubmitted = true;

    // stop here if form is invalid
    if (this.deleteProductForm.invalid) {
      return;
    }

    this.loading = true;
    const quantity = this.coffeeMachine.version === '1.1' ? this.dpf.quantity.value : 0;
    this.productsService.deleteProduct(this.coffeeMachine.id, this.productSelected.libelle, quantity).then(() => {
      this.fillProductTable();
      this.loading = false;
      this.modalService.dismissAll();
      this.deleteProductSubmitted = false;
      this.toastr.success('Le produit a été correctement supprimé');
      this.refreshCoffeeMachine(this.coffeeMachine.id);
    }).catch((err) => {
      this.loading = false;
      this.refreshCoffeeMachine(this.coffeeMachine.id);
      this.modalService.dismissAll();
    });
  }

  exportFile() {
    switch (this.currentExport) {
      case 'file-pdf':
        this.productsService.exportPdf(this.coffeeMachine.id);
        return;
      case 'file-csv':
        this.productsService.exportCsv(this.coffeeMachine.id);
        return;
      case 'code':
        this.productsService.exportJson(this.coffeeMachine.id);
        return;
    }
    return;
  }

  isAdmin() {
    return this.authService.hasRole('ADMIN');
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
