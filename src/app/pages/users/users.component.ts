import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  dataSource: any;
  displayedColumns =  ['id', 'mail', 'firstname', 'lastname', 'role', 'actions'];
  userSelected: any;
  currentUser: any;
  createSubmitted = false;
  editSubmitted = false;
  createForm: FormGroup;
  editForm: FormGroup;
  config = {};
  loading = false;
  exports = [
    {value: 'file-pdf', viewValue: 'PDF'},
    {value: 'file-csv', viewValue: 'CSV'},
    {value: 'code', viewValue: 'JSON'}
  ];
  currentExport = this.exports[0].value;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(private usersService: UsersService,
              private modalService: NgbModal,
              private authService: AuthenticationService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
    this.config = {
      animationType: 'circleSwish',
      backdropBorderRadius: '3px'
    };
    this.fillTable();
  }

  fillTable() {
    this.usersService.getUsers()
    .then((users) => {
      this.dataSource = new MatTableDataSource(users);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
    .catch((err) => {
    });
  }

  // convenience getter for easy access to form fields
  get cf() { return this.createForm.controls; }
  get ef() { return this.editForm.controls; }

  openCreateModal(createModal) {
    this.modalService.open(createModal);
    this.createForm = this.formBuilder.group({
      mail: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  openEditModal(user, editModal) {
    this.userSelected = user;
    if (this.userSelected.id === this.currentUser.id) {
      this.editForm = this.formBuilder.group({
        firstname: [this.userSelected.firstname, Validators.required],
        lastname: [this.userSelected.lastname, Validators.required]
      });
    } else {
      this.editForm = this.formBuilder.group({
        firstname: [this.userSelected.firstname, Validators.required],
        lastname: [this.userSelected.lastname, Validators.required],
        role: [this.userSelected.role, Validators.required]
      });
    }
    this.modalService.open(editModal);
  }

  openDeleteModal(user, deleteModal) {
    this.userSelected = user;
    this.modalService.open(deleteModal);
  }

  onCreateSubmit() {
    this.createSubmitted = true;

    // stop here if form is invalid
    if (this.createForm.invalid) {
      return;
    }

    this.loading = true;
    this.usersService.register(this.cf.mail.value, this.cf.firstname.value,
      this.cf.lastname.value, this.cf.password.value, this.cf.confirmPassword.value)
      .then(() => {
        this.fillTable();
        this.loading = false;
        this.createSubmitted = false;
        this.modalService.dismissAll();
        this.toastr.success('L\'utilisateur a été correctement ajouté');
      })
      .catch((error) => {
        this.loading = false;
      });
  }

  onEditSubmit() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    const role = this.userSelected.id === this.currentUser.id ? this.userSelected.role : this.ef.role.value;

    this.loading = true;
    this.usersService.editUser(this.userSelected.id, this.ef.firstname.value, this.ef.lastname.value, role)
    .then((user) => {
      this.loading = false;
      this.editSubmitted = false;
      this.fillTable();
      this.modalService.dismissAll();
      if (this.userSelected.id === this.currentUser.id) {
        const localUser = JSON.parse(localStorage.getItem('currentUser'));
        const modifiedUser = user;
        modifiedUser.authdata = localUser.authdata;
        localStorage.setItem('currentUser', JSON.stringify(modifiedUser));
        this.currentUser = modifiedUser;
      }
      this.toastr.success('L\'utilisateur a été correctement modifié');
    }).catch((err) => {
      this.loading = false;
    });
  }

  onDeleteSubmit() {
    this.loading = true;
    this.usersService.deleteUser(this.userSelected.id).then((user) => {
      if (this.currentUser.id === this.userSelected.id) {
        this.authService.logout();
      } else {
        this.loading = false;
        this.fillTable();
      }
      this.modalService.dismissAll();
      this.toastr.success('L\'utilisateur a été correctement supprimé');
    }).catch((err) => {
      this.loading = false;
    });
  }

  exportFile() {
    switch (this.currentExport) {
      case 'file-pdf':
        this.usersService.exportPdf();
        return;
      case 'file-csv':
        this.usersService.exportCsv();
        return;
      case 'code':
        this.usersService.exportJson();
        return;
    }
    return;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
