import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  user: any;
  currentUser: any;
  editSubmitted = false;
  editForm: FormGroup;
  config = {};
  loading = false;

  constructor(private route: ActivatedRoute,
              private usersService: UsersService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private authService: AuthenticationService,
              private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.config = {
      animationType: 'circleSwish',
      backdropBorderRadius: '3px'
    };
    this.currentUser = this.authService.getCurrentUser();
    const id = this.route.snapshot.params.id;
    this.usersService.getUserById(id)
    .then((user) => {
      this.user = user;
    }).catch((err) => {
    });
  }

  get ef() { return this.editForm.controls; }

  openEditModal(editModal) {
    this.modalService.open(editModal);
    if (this.user.id === this.currentUser.id) {
      this.editForm = this.formBuilder.group({
        firstname: [this.user.firstname, Validators.required],
        lastname: [this.user.lastname, Validators.required]
      });
    } else {
      this.editForm = this.formBuilder.group({
        firstname: [this.user.firstname, Validators.required],
        lastname: [this.user.lastname, Validators.required],
        role: [this.user.role, Validators.required]
      });
    }
  }

  openDeleteModal(deleteModal) {
    this.modalService.open(deleteModal);
  }

  onEditSubmit() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    const role = this.user.id === this.currentUser.id ? this.user.role : this.ef.role.value;

    this.loading = true;
    this.usersService.editUser(this.user.id, this.ef.firstname.value, this.ef.lastname.value, role)
    .then((user) => {
      this.loading = false;
      this.editSubmitted = false;
      this.user = user;
      this.modalService.dismissAll();
      if (this.user.id === this.currentUser.id) {
        const localUser = JSON.parse(localStorage.getItem('currentUser'));
        const modifiedUser = user;
        modifiedUser.authdata = localUser.authdata;
        localStorage.setItem('currentUser', JSON.stringify(modifiedUser));
        this.currentUser = modifiedUser;
      }
      this.toastr.success('L\'utilisateur a été correctement modifié');
    }).catch((err) => {
      this.loading = false;
    });
  }

  onDeleteSubmit() {
    this.loading = true;
    this.usersService.deleteUser(this.user.id).then((user) => {
      if (this.currentUser.id === this.user.id) {
        this.authService.logout();
      } else {
        this.loading = false;
        this.router.navigate(['/users']);
      }
      this.modalService.dismissAll();
      this.toastr.success('L\'utilisateur a été correctement supprimé');
    }).catch((err) => {
      this.loading = false;
    });
  }
}
