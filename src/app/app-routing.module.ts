import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { UsersComponent } from './pages/users/users.component';
import { ForbiddenComponent } from './pages/forbidden/forbidden.component';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { CoffeeMachinesComponent } from './pages/coffee-machines/coffee-machines.component';
import { CoffeeMachineDetailsComponent } from './pages/coffee-machines/coffee-machine-details/coffee-machine-details.component';
import { UserDetailsComponent } from './pages/users/user-details/user-details.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'users', component: UsersComponent, canActivate: [RoleGuard, AuthGuard], data: { role: 'ADMIN' } },
  { path: 'users/:id', component: UserDetailsComponent, canActivate: [RoleGuard, AuthGuard], data: { role: 'ADMIN' } },
  { path: 'profile', canActivate: [AuthGuard], component: ProfileComponent },
  { path: 'coffeemachines', canActivate: [AuthGuard], component: CoffeeMachinesComponent },
  { path: 'coffeemachines/:id', canActivate: [AuthGuard], component: CoffeeMachineDetailsComponent },
  { path: '403', component: ForbiddenComponent },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
