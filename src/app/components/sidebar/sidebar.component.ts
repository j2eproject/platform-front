import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  isAuthenticated: boolean;
  authenticatedSubscription: Subscription;

  constructor(private authService: AuthenticationService) {
    this.authenticatedSubscription = this.authService.authenticatedSubject.subscribe(
      (isAuthenticated: boolean) => {
        this.isAuthenticated = isAuthenticated;
      }
    );
    this.authService.setAuthenticated(false);
  }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  logout() {
    this.authService.logout();
  }

  isAdmin() {
    return this.authService.hasRole('ADMIN');
  }

  isUser() {
    return this.authService.hasRole('USER');
  }

}
