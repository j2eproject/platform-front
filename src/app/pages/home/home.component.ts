import { Component, OnInit, PipeTransform, Pipe, SecurityContext } from '@angular/core';
import { WidgetsService } from 'src/app/services/widgets.service';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  url = 'https://www.infoclimat.fr/public-api/mixed/iframeSLIDE?_ll=48.85341,2.3488&_inc=WyJQYXJpcyIsIjQyIiwiMjk4ODUwNyIsIkZSIl0=&_auth=ARtVQlMtAyEDLlNkBHIELQJqADVcKgUiAn4FZg1oVypTOFEwVjYBZ18xBHlSfQUzUXxTMF5lCTkFbgN7CHoCYwFrVTlTOANkA2xTNgQrBC8CLABhXHwFIgJiBWUNflc1UzdRNlYrAWJfMQRjUnwFMFFgUzFefgkuBWcDYQhiAmkBY1U3UzMDYANkUzgEKwQvAjQANVxiBT0CNQUwDTZXMVNjUTZWYAE3X2UEYVJ8BTBRY1M3XmQJOAVkA2UIZwJ%2BAX1VSFNDA3wDLFNzBGEEdgIsADVcPQVp&_c=efa61d6ed6ce86fdd31c0f5f98ab4d74';

  constructor(private widgetsService: WidgetsService) { }

  ngOnInit() {
    this.weekEnd();
  }

  weekEnd() {
    // this.widgetsService.getWeekend();
    // this.widgetsService.getWeekend();.then((res) => console.log(res)).catch((err) => console.log(err));
    // this.widgetsService.getWeekend().subscribe((res) => console.log(res));
  }

}
