export const environment = {
  production: true,
  environmentName: 'PROD',
  API_ENDPOINTS: {
    PLATFORM: 'http://onsecopie.com/api'
  }
};
