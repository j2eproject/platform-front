import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  editSubmitted = false;
  editPasswordSubmitted = false;
  editForm: FormGroup;
  editPasswordForm: FormGroup;
  config = {};
  loading = false;

  constructor(private route: ActivatedRoute,
              private usersService: UsersService,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private authService: AuthenticationService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.config = {
      animationType: 'circleSwish',
      backdropBorderRadius: '3px'
    };
    this.user = this.authService.getCurrentUser();
  }

  get ef() { return this.editForm.controls; }
  get epf() { return this.editPasswordForm.controls; }

  openEditModal(editModal) {
    this.modalService.open(editModal);
    this.editForm = this.formBuilder.group({
      firstname: [this.user.firstname, Validators.required],
      lastname: [this.user.lastname, Validators.required],
      role: [this.user.role, Validators.required]
    });
  }

  openEditPasswordModal(editPasswordModal) {
    this.modalService.open(editPasswordModal);
    this.editPasswordForm = this.formBuilder.group({
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  onEditSubmit() {
    this.editSubmitted = true;

    // stop here if form is invalid
    if (this.editForm.invalid) {
      return;
    }

    this.loading = true;
    this.usersService.editUser(this.user.id, this.ef.firstname.value, this.ef.lastname.value, this.user.role)
    .then((user) => {
      this.loading = false;
      this.editSubmitted = false;
      this.user = user;
      this.modalService.dismissAll();
      const authdata = JSON.parse(localStorage.getItem('currentUser')).authdata;
      user.authdata = authdata;
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.toastr.success('Vous avez bien modifié votre profil');
    }).catch((err) => {
      this.loading = false;
    });
  }

  onEditPasswordSubmit() {
    this.editPasswordSubmitted = true;

    // stop here if form is invalid
    if (this.editPasswordForm.invalid) {
      return;
    }

    this.loading = true;


    this.usersService.editPasswordUser(this.user.id, this.epf.password.value, this.epf.confirmPassword.value)
    .then(() => {
      let user = JSON.parse(localStorage.getItem('currentUser'));
      this.loading = false;
      this.editPasswordSubmitted = false;
      this.modalService.dismissAll();
      user.authdata = window.btoa(user.mail + ':' + this.epf.password.value);
      localStorage.setItem('currentUser', JSON.stringify(user));
      this.toastr.success('Vous avez bien changé votre mot de passe');
    }).catch((error) => {
      this.loading = false;
    });
  }

}
