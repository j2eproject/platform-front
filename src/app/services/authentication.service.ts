import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';
import { Router } from '@angular/router';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    authenticatedSubject: Subject<boolean>;

    constructor(private http: HttpClient, private configService: ConfigService, private router: Router) {
        this.authenticatedSubject = new Subject<boolean>();
    }

    login(mail: string, password: string): Promise<any> {
        return this.http.post<any>(`${this.configService.platformApi()}/authenticate`,
            {
                mail, password
            }).toPromise();
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
        this.setAuthenticated(false);
    }

    isAuthenticated() {
        if (localStorage.getItem('currentUser')) {
            return true;
        }
        return false;
    }

    getCurrentUser(): any {
        const currentUser = localStorage.getItem('currentUser');
        if (currentUser) {
            return JSON.parse(currentUser);
        }
        return null;
    }

    hasRole(role: string) {
        if (this.isAuthenticated()) {
            return this.getCurrentUser().role === role;
        }
        return false;
    }

    setAuthenticated(authenticated: boolean) {
        this.authenticatedSubject.next(authenticated);
    }
}
