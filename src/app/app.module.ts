import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ForbiddenComponent } from './pages/forbidden/forbidden.component';
import { LoginComponent } from './pages/login/login.component';
import { ConfigService } from './services/config.service';
import { AuthenticationService } from './services/authentication.service';
import { UsersService } from './services/users.service';
import { HomeComponent, SafePipe } from './pages/home/home.component';
import { UsersComponent } from './pages/users/users.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ErrorInterceptor } from './helpers/error.interceptor';
import { BasicAuthInterceptor } from './helpers/basic-auth.interceptor';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbModule, NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import {
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule, MatSelectModule, MatButtonModule
} from '@angular/material';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RegisterComponent } from './pages/register/register.component';
import { WidgetsService } from './services/widgets.service';
import { CoffeeMachinesComponent } from './pages/coffee-machines/coffee-machines.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { CoffeeMachinesService } from './services/coffee-machines.service';
import { CoffeeMachineDetailsComponent } from './pages/coffee-machines/coffee-machine-details/coffee-machine-details.component';
import { UserDetailsComponent } from './pages/users/user-details/user-details.component';
import { ProductsService } from './services/products.service';

@NgModule({
  declarations: [
    AppComponent,
    SafePipe,
    PageNotFoundComponent,
    ForbiddenComponent,
    UsersComponent,
    LoginComponent,
    HomeComponent,
    SidebarComponent,
    RegisterComponent,
    CoffeeMachinesComponent,
    ProfileComponent,
    CoffeeMachineDetailsComponent,
    UserDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    NgbModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatButtonModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true },
    ConfigService,
    AuthenticationService,
    UsersService,
    CoffeeMachinesService,
    WidgetsService,
    ProductsService,
    AuthGuard,
    RoleGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
